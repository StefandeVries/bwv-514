upper = {
	\key c \major
	\time 3/4
	\clef "treble_8"
	
	<e' g'>2 <c' g'>4
	<a c'> <b d'> <c' g'>
	<a c'>4. d'8 <b d' g'>4
	<g c' e'>2.
	
	<c' e'>4 <d' fis'> <g f'>
	<c' e'>2 a'4
	<d' g'>4 <d' fis'> d'
	<b d>2.
	
	<g c' e'>2 <g c' g'>4
	<a c'>4 <d' f'>4 <e' g'>4
	<a c'>4. d'8 g'4
	<c' e'>4 <b d'> <e g c'>
	
	<g c'>4. <a d'>8 <g d'>4
	<a c' e'>2 <fis d'>4
	g8 b d4 d 
	<b d'>2.
	
	c'4 g g
	<a f'>4 <g f'>4 <g c'>
	<d g>4. b8 <c g>4
	<g d'>2.
	
	c'4 g' <d' g'>4
	<a c'>4 <b d'> <b g'>
	<f c'>4 <f d'>2
	<g c' e'>2.
	
	% repetition
	
	<e' g'>2 <c' g'>4
	<a c'> <b d'> <c' g'>
	<a c'>4. d'8 <b d' g'>4
	<g c' e'>2.
	
	<c' e'>4 <d' fis'> <g f'>
	<c' e'>2 a'4
	<d' g'>4 <d' fis'> d'
	<b d>2.
	
	<g c' e'>2 <g c' g'>4
	<a c'>4 <d' f'>4 <e' g'>4
	<a c'>4. d'8 g'4
	<c' e'>4 <b d'> <e g c'>
	
	<g c'>4. <a d'>8 <g d'>4
	<a c' e'>2 <fis d'>4
	g8 b d4 d 
	<b d'>2.
	
	c'4 g g
	<a f'>4 <g f'>4 <g c'>
	<d g>4. b8 <c g>4
	<g d'>2.
	
	c'4 g' <d' g'>4
	<a c'>4 <b d'> <b g'>
	<f c'>4 <f d'>2
	<g c' e'>2.
}

lower = {
	\key c \major
	\time 3/4
	\clef bass
	
	c4 b, g,
	f4~ <f g>4 e4
	d4 d g,
	c4. g,8 c,4
	
	c4. c8 b,4
	a,4 g, fis,
	g,4 d fis,
	g4 g,8\( a, b, c\)
	
	c4 d e
	f2 e4
	d4 d g,
	c4 g c,
	
	c4. c8 b,4
	a,4 g, fis,
	g,8\( b, d fis\) d4
	<g, g>4 a,8\( b, c d\)
	
	e4 d8 b, c e
	f4 f e
	b,4 g, c,
	g, g8\( a b g\)
	
	c8 f16 g c'4 e
	f4 e c
	a,4 f8 a g b
	c2.
	
	% repetition
	
	c4 b, g,
	f4~ <f g>4 e4
	d4 d g,
	c4. g,8 c,4
	
	c4. c8 b,4
	a,4 g, fis,
	g,4 d fis,
	g4 g,8\( a, b, c\)
	
	c4 d e
	f2 e4
	d4 d g,
	c4 g c,
	
	c4. c8 b,4
	a,4 g, fis,
	g,8\( b, d fis\) d4
	<g, g>4 a,8\( b, c d\)
	
	e4 d8 b, c e
	f4 f e
	b,4 g, c,
	g, g8\( a b g\)
	
	c8 f16 g c'4 e
	f4 e c
	a,4 f8 a g b
	c2.
}
