singer = {
	\set Staff.instrumentName = #"Voice"
	\set Staff.midiInstrument = #"flute" % just to be able to "hear" the voice more distinctly in the WAVE-rendering
	
	\clef treble
	\time 3/4
	\key c \major
	
	c''4 b' c''
	a'4\( b'\) c''
	f'4.\( g'16 a'16\) g'4
	e'4\( d'8 e'\) c'4
	
	g'4 a' b'
	c''2 e''4
	b'8\( a'\) a'2
	g'2.
	
	c''4 b' c''
	a'\( b'\) c''
	f'4.\( g'16 a'16\) g'4
	e'4\( d'8 e'\) c'4
	
	g'4 a' b'
	c''2 d''4
	b'8\( a'\) a'2
	g'2.
	
	c''4 d'' e''
	a'4\( b'\) c''
	d''4.\( e''16 f''\) e''4 
	d''2.
	
	e''8\( f''16 g''\) f''8[\( e''\)] d''[\( c''\)]
	a'4\( b'\) c''
	f''4 d''2
	c''2.
	
	% repetition
	
	c''4 b' c''
	a'4\( b'\) c''
	f'4.\( g'16 a'16\) g'4
	e'4\( d'8 e'\) c'4
	
	g'4 a' b'
	c''2 e''4
	b'8\( a'\) a'2
	g'2.
	
	c''4 b' c''
	a'\( b'\) c''
	f'4.\( g'16 a'16\) g'4
	e'4\( d'8 e'\) c'4
	
	g'4 a' b'
	c''2 d''4
	b'8\( a'\) a'2
	g'2.
	
	c''4 d'' e''
	a'4\( b'\) c''
	d''4.\( e''16 f''\) e''4 
	d''2.
	
	e''8\( f''16 g''\) f''8[\( e''\)] d''\([ c''\)]
	a'4\( b'\) c''
	f''4 d''2
	c''2.
}

text = \lyricmode
{
    Schaffs mit mir Go- ott, nach dei - - nen Wi - - len,
    dir sei es al- les
    hei- m- ge- stellt.
    Du wirst mein Wue- n- schen so - - er- fuel - - len,
    wie's dei- ner Weis- heit wo- hl ge- faellt.
    Du bist mein Va - ter,
    du - - wirst mich
    ver - - sor - gen -,
    dar - auf hof- fe
    ich.
    
    Zu dir, mein Gott -, steht mein - - Ver- trau - - en,
    du bist mein Gott -, mein Heil -, mein Schutz.
    Auf idch will ich- be- staen - - dig
    bau - - en,
    mit dir biet ich dem Teu - fel Trutz.
    Ist Gott fuer mi- ch und blei - - bet mein,
    we- - r ma- g mi- r dann - zu- wi- der sein.
}
