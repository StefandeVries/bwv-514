\version "2.16.2"
\pointAndClickOff
\include "singer.ly"
\include "piano.ly"

\header {
	title = "Schaffs mit mir, Gott"
	subtitle = "BWV 514"
	%composer = "J.S. Bach"
	arranger = "Stefan de Vries"
	tagline = \markup { \tiny \smaller \italic "LilyPond 2.16.2"}
}

\score {
	<<
		\new Voice= "singer" \singer
		\new Lyrics \lyricsto singer \text
		\new PianoStaff 
		<<
			\set PianoStaff.instrumentName = #"Piano"
			\new Staff = "upper" \upper
			\new Staff = "lower" \lower
		>>
	>>
	\midi{}
	\layout{}
}
